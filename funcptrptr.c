#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int doub(int);
int trip(int);
int half(int);
void fpv_exec(int (**fpv)(int), int n);
void fpvv_exec(int (***fpvv)(int), int n);
void fpvvv_exec(int (****fpvvv)(int), int n);

int doub(int i) {
	return i * 2;
}

int trip(int i) {
	return i * 3;
}

int half(int i) {
	return i / 2;
}

void fpv_exec(int (**fpv)(int), int n) {
	int i;
	for (i = 0; fpv[i]; i++)
		printf("%d\n", (fpv[i])(n));
}

void fpvv_exec(int (***fpvv)(int), int n) {
	int i;
	for (i = 0; fpvv[i]; i++)
		fpv_exec(fpvv[i], n);
}

void fpvvv_exec(int (****fpvvv)(int), int n) {
	int i;
	for (i = 0; fpvvv[i]; i++)
		fpvv_exec(fpvvv[i], n);
}

int main(int argc, char **argv) {
	int (****fpvvv)(int);
	int (*fpv[])(int) = {doub, trip, half, NULL};
	int i, j, im, jm;
	im = jm = 2;

	fpvvv = calloc(im + 1, sizeof *fpvvv);
	for (i = 0; i < im; i++) {
		fpvvv[i] = calloc(jm + 1, sizeof **fpvvv);
		for (j = 0; j < jm; j++) {
			fpvvv[i][j] = malloc(sizeof fpv);
			memcpy(fpvvv[i][j], fpv, sizeof fpv);
		}
		fpvvv[i][jm + 1] = NULL;
	}
	fpvvv[im + 1] = NULL;

	for (argv++, argc--; argc; argv++, argc--)
		fpvvv_exec(fpvvv, atoi(*argv));

	exit(EXIT_SUCCESS);
}

#include <stdio.h>
#include <stdlib.h>

int doub(int);
int trip(int);
int half(int);
static int (*fpv[4])(int);
void fpv_exec(int (**)(int), int);

int doub(int i) {
	return i * 2;
}

int trip(int i) {
	return i * 3;
}

int half(int i) {
	return i / 2;
}

static int (*fpv[4])(int) = {doub, trip, half, NULL};

void fpv_exec(int (**fpv)(int), int n) {
	while (*fpv)
		printf("%d\n", (*fpv++)(n));
}

int main(int argc, char **argv) {
	for (argv++, argc--; argc; argv++, argc--)
		fpv_exec(fpv, atoi(*argv));
	exit(EXIT_SUCCESS);
}

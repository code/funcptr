.POSIX:
.PHONY: all clean
ALL = funcptr funcptrptr
all: $(ALL)
clean:
	rm -f $(ALL)
